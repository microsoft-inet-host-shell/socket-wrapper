﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Socket_Wrapper
{
    public class SocketWrapper
    {
         private DateTime start;

        public Int64 totalDataToSendB { get; set; }

        public Int64 packetSizeB { get; set; }
        public Int64 totalTimeS { get; set; }
        public Int64 delayMS { get; set; }
        public string targetIP { get; set; }
        public int port { get; set; }

        public byte[] dataBytes;

        public SocketWrapper()
        {
            resetParameters();
        }

        public void resetParameters() 
        {
            totalDataToSendB = 0; // Default disable, if > 0 then override totalTimeS
            packetSizeB = 4 * 1024; // Default 4KB in bytes
            totalTimeS = 2 * 60 * 60; // Default 2 Hours in seconds
            delayMS = 250; // Default 250 in milliseconds
            targetIP = String.Empty; // IP Address of host
            port = 0; // Default 0 = random ports
            dataBytes = getBytes();
            start = DateTime.Now;
        }

        public void startSending()
        {
            if (targetIP == String.Empty)
                return;
            
            if (port < 1 || port > 65535)
                port = 80;

            if (totalTimeS < 0)
                totalTimeS = 1 * 60 * 60;

            if (delayMS < 100)
                delayMS = 100;

            if (packetSizeB < 0)
                packetSizeB = 4 * 1024;

            if (packetSizeB > 50 * 1024)
                packetSizeB = 50 * 1024;

            if (totalDataToSendB < 0)
                totalDataToSendB = 0;


            start = DateTime.Now;

            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            client.SendTimeout = 100000;
            int totalDataSentB = 0;
            int retries = 0;
            while (true)
            {
                System.Threading.Thread.Sleep(int.Parse(delayMS.ToString()));
                try
                {
                    if (port == 0)
                        port = getRandomPort();
                    dataBytes = getBytes();

                    IPEndPoint ep = new IPEndPoint(IPAddress.Parse(targetIP), port);
                    
                    client.SendTo(dataBytes, ep);

                    totalDataSentB += dataBytes.Length;

                    if (totalDataToSendB != 0)
                    {
                        if (totalDataSentB >= totalDataToSendB)
                            break;
                    }
                    else if ((DateTime.Now - start).TotalSeconds >= totalTimeS)
                        break;
                    retries = 0;
                }
                catch (Exception e)
                {
                    retries++;
                    if (retries > 20)
                    {
                        return;
                    }
                }
            }

        }

        private byte[] getBytes() {
            Random random = new Random();
            byte[] dataBytes;
            if (packetSizeB > 0)
            {
                dataBytes = new byte[packetSizeB];
            }
            else
            {
                dataBytes = new byte[random.Next(30)];
            }

            random.NextBytes(dataBytes);
            return dataBytes;
        }

        private int getRandomPort()
        {
            Random random = new Random();
            return random.Next(65535);
        }

        static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        static string SizeSuffix(Int64 value, int decimalPlaces = 1)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }
    }
}
